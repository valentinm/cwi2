class ChatwillsController < ApplicationController
  before_action :set_chatwill, only: [:show, :edit, :update, :destroy]

  # GET /chatwills
  # GET /chatwills.json
  def index
    @chatwills = Chatwill.all
  end

  # GET /chatwills/1
  # GET /chatwills/1.json
  def show
  end

  # GET /chatwills/new
  def new
    @chatwill = Chatwill.new
  end

  # GET /chatwills/1/edit
  def edit
  end

  # POST /chatwills
  # POST /chatwills.json
  def create
    @chatwill = Chatwill.new(chatwill_params)

    respond_to do |format|
      if @chatwill.save
        format.html { redirect_to @chatwill, notice: 'Chatwill was successfully created.' }
        format.json { render :show, status: :created, location: @chatwill }
      else
        format.html { render :new }
        format.json { render json: @chatwill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /chatwills/1
  # PATCH/PUT /chatwills/1.json
  def update
    respond_to do |format|
      if @chatwill.update(chatwill_params)
        format.html { redirect_to @chatwill, notice: 'Chatwill was successfully updated.' }
        format.json { render :show, status: :ok, location: @chatwill }
      else
        format.html { render :edit }
        format.json { render json: @chatwill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /chatwills/1
  # DELETE /chatwills/1.json
  def destroy
    @chatwill.destroy
    respond_to do |format|
      format.html { redirect_to chatwills_url, notice: 'Chatwill was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_chatwill
      @chatwill = Chatwill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def chatwill_params
      params.require(:chatwill).permit(:sentence, :author, :date)
    end
end

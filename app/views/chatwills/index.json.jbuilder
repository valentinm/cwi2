json.array!(@chatwills) do |chatwill|
  json.extract! chatwill, :id, :sentence, :author, :date
  json.url chatwill_url(chatwill, format: :json)
end

class CreateChatwills < ActiveRecord::Migration
  def change
    create_table :chatwills do |t|
      t.string :sentence
      t.string :author
      t.datetime :date

      t.timestamps null: false
    end
  end
end

require 'test_helper'

class ChatwillsControllerTest < ActionController::TestCase
  setup do
    @chatwill = chatwills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:chatwills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create chatwill" do
    assert_difference('Chatwill.count') do
      post :create, chatwill: { author: @chatwill.author, date: @chatwill.date, sentence: @chatwill.sentence }
    end

    assert_redirected_to chatwill_path(assigns(:chatwill))
  end

  test "should show chatwill" do
    get :show, id: @chatwill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @chatwill
    assert_response :success
  end

  test "should update chatwill" do
    patch :update, id: @chatwill, chatwill: { author: @chatwill.author, date: @chatwill.date, sentence: @chatwill.sentence }
    assert_redirected_to chatwill_path(assigns(:chatwill))
  end

  test "should destroy chatwill" do
    assert_difference('Chatwill.count', -1) do
      delete :destroy, id: @chatwill
    end

    assert_redirected_to chatwills_path
  end
end
